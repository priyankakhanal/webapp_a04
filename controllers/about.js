const express = require('express')
const api = express.Router()

api.get('/t4', (req, res) => {
    console.log(`Handling GET ${req}`)
    res.render('about/t4/index.ejs',
          { title: 'WebApps A04', layout: 'layout.ejs' })
  })
  api.get('/t4/a', (req, res) => {
    console.log(`Handling GET ${req}`)
    res.render('about/t4/a/index.ejs',
          { title: 'Swathi Challagundla', layout: 'layout.ejs' })
  })
  api.get('/t4/b', (req, res) => {
    console.log(`Handling GET ${req}`)
    return res.render('about/t4/b/index.ejs',
          { title: 'Laxmi Seshu Kalvakuri', layout: 'layout.ejs' })
  })
  api.get('/t4/c', (req, res) => {
    console.log(`Handling GET ${req}`)
    return res.render('about/t4/c/index.ejs',
          { title: 'Priyanka Khanal', layout: 'layout.ejs' })
  })
  api.get('/t4/d', (req, res) => {
    console.log(`Handling GET ${req}`)
    return res.render('about/t4/d/index.ejs',
          { title: 'Durga Susmitha Kotyada', layout: 'layout.ejs'})
  })
  module.exports = api