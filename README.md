Group 6 -04 E-Commerce A04 App

Overview: Web Apps and Services Group Ecommerce App Project

Team:

Customer - Lakshmi Sesh Kalavakuri
Product - Durga Susmitha Kotyada
Order - Priyanka Khanal
Order Line - Swathi

Includes:
Customer
Products
Orders
OrderLine

Uses:
JSON
JavaScript
EJS

Instructions:
Initially we need to Run the app locally by using the command node app.js
In index.js choose an action to GET a view or to GET JSON
Use available buttons to create, edit,delete or view the details.


