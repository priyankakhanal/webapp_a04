/** 
*  Order model
*  Describes the characteristics of each attribute in an order resource.
*
* @author Priyanka Khanal <s524702@nwmissouri.edu>
*
*/

// bring in mongoose 
// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const OrderSchema = new mongoose.Schema({

  _id: { type: Number,
     required: true 
    },
  email: {
    type: String,
    required: true
  },
  datePlaced:
   {
    type: Date,
    required: true,
    default: Date.now()
  },
  dateShipped: 
  {
    type: Date,
    required: false
  },
  paymentType: {
    type: String,
    enum: ['not selected yet', 'Cards', 'Cash'],
    required: true,
    default: 'not selected yet'
  },
  paid: {
    type: Boolean,
    default: false
  }
})
module.exports = mongoose.model('order', OrderSchema)