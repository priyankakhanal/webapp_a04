/** 
*  Product model
*  Describes the characteristics of each attribute in a product resource.
*
*  @author Durga Sushmitha Kotyada <s533710@nwmissouri.edu>
*
*/

// bring in mongoose 
// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const ProductSchema = new mongoose.Schema({

  _id: { type: Number, required: true },
  Product_Name: {
    type: String,
    required: true,
    unique: true
  },
  Product_Description: {
    type: String,
    required: true,
    default: ''
  },
  Product_Price: {
    type: Number,
    required: true,
    default: 0.0
  },
  Product_Quality: {
   type : String,
   required : true,
   default : ''
},
Product_Status: {
    type: String,
    required: true,
    default: 'Available'
  },
  Product_ManfactureDate: {
    type: String,
    required: false,
    default: ''
  },
  Product_ExpiryDate: {
    type: String,
    required: true,
    default: ''
  },
  
})
module.exports = mongoose.model('Product', ProductSchema)