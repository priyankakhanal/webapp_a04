/** 
*  Customer model
*  Describes the characteristics of each attribute in a customer resource.
*
* @author Lakshmi Seshu Kalvakuri<S533707@nwmissouri.edu>
*
*/

// bring in mongoose 
// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const CustomerSchema = new mongoose.Schema({

    _id: {
        type: Number,
        required: true,
        description: "The unique identifier to identify a Customer"
    },

    firstName: {
        type: String,
        required: true,
        default: 'First Name'

    },
    lastName: {
        type: String,
        required: true,
        default: 'Last Name'

    },
    age: {
        type: Number,
        required: true

    },
   
    city: {
        type: String,
        required: true,
        default: 'Maryville'
    },
    state: {
        type: String,
        required: true,
        default: 'MO'
    },
    postalCode: {
        type: String,
        required: true,
        default: '64468'
    },

phoneNumber: {
    type: String,
        required: true

}
})
module.exports = mongoose.model('Customer', CustomerSchema)
